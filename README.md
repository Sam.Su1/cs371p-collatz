# CS371p: Object-Oriented Programming Collatz Repo

* Name: Yiheng Su

* EID: ys22933

* GitLab ID: sam.su1

* HackerRank ID: samsoup2018

* Git SHA: adcf7dc4c1074fd037bbd13d725627fb93df0c2f

* GitLab Pipelines: https://gitlab.com/Sam.Su1/cs371p-collatz/pipelines

* Estimated completion time: 25

* Actual completion time: 14

* Comments: Edge cases are trivial but are hard to discover
