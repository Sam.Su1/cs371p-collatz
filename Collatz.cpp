// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <climits> // LONG_MIN
#include <algorithm> // std:max
#include "Collatz.hpp"

// how big our lazy_cache would be
#define MAX 100000
using namespace std;

// Global: stores the cycle-length of each index (as an int), 0ed out
static long lazy_cache[MAX] = {};

// ------------
// collatz_read
// ------------

istream& collatz_read (istream& r, int& i, int& j)
{
    return r >> i >> j;
}

//	Computes the cycle length of a particular num
long cycle_length (long num)
{
    long length = 1;  // not int to avoid overflow
    while (num != 1)
    {
        // make sure that we are in bounds of the array
        if (num < MAX && num >= 0)
        {
            // at this point, num must be valid
            assert(num >= 0 && num < MAX);
            // already computed?, we can just return, -1 to minus self
            if (lazy_cache[num] != 0)
                return length + lazy_cache[num] - 1;
        }
        if (num % 2 == 0)
            num /= 2;
        else
        {
            // odd, we can simply change (3n+1)/2 to (n + 1/2n + 1)
            num = num+num/2+1;
            // we must do extra increment as we divided by 2
            ++length;
        }
        ++length;
    }
    return length;
}

// ------------
// collatz_eval
// This function evaluates the max cycle length between i and j
// By computing all num's cycle length from i to j (or j to i),
// which ever is bigger. Each num's cycle length is subsequently
// stored in a lazy_cache to avoid re-calculations by index.
// ------------

int collatz_eval (int i, int j)
{
    // check pre-conditions
    assert(i > 0 && j > 0);
    assert(i < 1000000 && j < 1000000);

    // stores the max cycle_length between this range
    // this is also the return value
    long max = LONG_MIN;
    // j is not guranteed to be greater than j
    if (j <= i)
    {
        // so we swap i and j if j is smaller!
        int temp = i;
        i = j;
        j = temp;
    }
    while (i <= j)
    {
        long length;
        // already computed this before? don't do it again!
        if (i < MAX && lazy_cache[i] != 0)
        {
            // check index-validity
            assert(i >= 0 && i < MAX);
            length = lazy_cache[i];
        }
        else
        {
            // compute the cycle length of this number
            length = cycle_length ((long) i);
            // can we store this cycle_length? if so cache it
            if (i < MAX)
                lazy_cache[i] = length;
        }
        // update return value is new cycle length is bigger
        max = std::max (length, max);
        ++i;
    }

    // check return value validity
    assert (max >= 1);
    return max;
}


// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v)
{
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w)
{
    int i;
    int j;
    while (collatz_read(r, i, j))
    {
        const int v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
