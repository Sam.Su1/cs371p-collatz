// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl, istream
#include <sstream>  // istringtstream, ostringstream

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    istringstream r("1 10\n");
    int i;
    int j;
    istream& s = collatz_read(r, i, j);
    ASSERT_TRUE(s);
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
}

// ----
// eval Unit tests
// ----

TEST(CollatzFixture, eval_1) {
    const int v = collatz_eval(1, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval_2) {
    const int v = collatz_eval(100, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval_3) {
    const int v = collatz_eval(201, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval_4) {
    const int v = collatz_eval(900, 1000);
    ASSERT_EQ(v, 174);
}

// the cycle length between ranges of the same number should be itself
TEST(CollatzFixture, eval_5) {
    const int v = collatz_eval(5, 5);
    ASSERT_EQ(v, 6);
}

// the cycle length of largest possible input should be 259
TEST(CollatzFixture, eval_6) {
    const int v = collatz_eval(999999, 999999);
    ASSERT_EQ(v, 259);
}

// the cycle length of smallest possible input should be 2
TEST(CollatzFixture, eval_7) {
    const int v = collatz_eval(1, 1);
    ASSERT_EQ(v, 1);
}

// // invalid inputs should fail, confirmed
// TEST(CollatzFixture, eval_8) {
//     const int v = collatz_eval(0, 2);
//     ASSERT_EQ(v, 2);}

// general large number case
TEST(CollatzFixture, eval_8) {
    const int v = collatz_eval(227432, 847881);
    ASSERT_EQ(v, 525);
}

// flipping the inputs should have no affect
TEST(CollatzFixture, eval_9) {
    const int v = collatz_eval(847881, 227432);
    ASSERT_EQ(v, 525);
}

// longest case possible: smallest to largest input
TEST(CollatzFixture, eval_10) {
    const int v = collatz_eval(1, 999999);
    ASSERT_EQ(v, 525);
}

// flipping the inputs should have no affect
TEST(CollatzFixture, eval_11) {
    const int v = collatz_eval(999999, 1);
    ASSERT_EQ(v, 525);
}

// three random number cases
TEST(CollatzFixture, eval_12) {
    const int v = collatz_eval(805442, 330326);
    ASSERT_EQ(v, 509);
}

// flipping the inputs should have no affect
TEST(CollatzFixture, eval_13) {
    const int v = collatz_eval(271394, 139196);
    ASSERT_EQ(v, 443);
}

// flipping the inputs should have no affect
TEST(CollatzFixture, eval_14) {
    const int v = collatz_eval(50916, 799329);
    ASSERT_EQ(v, 509);
}
// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream r("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", w.str());
}
